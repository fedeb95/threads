#!/bin/bash
FILE=$1
R=0
W=0
F=1000
C=0
LAST=""
for i in $(cat $FILE|grep acquired|cut -c1-1|sed \$d)
	do
		if [ "$i" == "r" ]
			then
				let "R+=1"	
			else
				let "W+=1"
		fi
		#if [ "$LAST" != "" ] && [ "$LAST" != "$i" ]
		#	then
				if [ "$W" -ne "0" ]
					then
						let "F+=R*1000/W"
						let "C+=1"
				fi
				if [ "$LAST" == "r" ]
					then
					 	R=0
					else
						W=0
				fi
		#fi
		LAST=$i
	done
let "F=F/C"
echo $F
