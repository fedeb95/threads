#include <semaphore.h>
#include <stdio.h>
#include <pthread.h>

typedef struct _rwlock_t{
	sem_t lock;
	sem_t writelock;
	pthread_cond_t cond;
	pthread_mutex_t c_mutex;
	int waiting;
	int readers;
}rwlock_t;

void rwlock_init(rwlock_t *rw){
	printf("fairness\n");
	rw->readers = 0;
	rw->waiting = 0;
	rw->cond = (pthread_cond_t)PTHREAD_COND_INITIALIZER;
	rw->c_mutex = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
	sem_init(&rw->lock, 0, 1);
	sem_init(&rw->writelock, 0, 1);
}

void rwlock_acquire_readlock(rwlock_t *rw){
	pthread_mutex_lock(&rw->c_mutex);
	if (rw->waiting > 0)
		pthread_cond_wait(&rw->cond, &rw->c_mutex);
	sem_wait(&rw->lock);
	pthread_mutex_unlock(&rw->c_mutex);
	rw->readers++;
	if (rw->readers == 1)
		sem_wait(&rw->writelock);
	sem_post(&rw->lock);
}

void rwlock_release_readlock(rwlock_t *rw){
	sem_wait(&rw->lock);
	rw->readers--;
	if (rw->readers == 0)
		sem_post(&rw->writelock);
	sem_post(&rw->lock);
}

void rwlock_acquire_writelock(rwlock_t *rw){
	rw->waiting++;
	sem_wait(&rw->writelock);
	rw->waiting--;
}

void rwlock_release_writelock(rwlock_t *rw){
	pthread_cond_signal(&rw->cond);
	sem_post(&rw->writelock);
}
