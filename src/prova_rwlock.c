#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "lib/rwlock.h"

typedef struct _args_t{
	char * array;
	int thnum;
}args_t;

rwlock_t rwlock;
sem_t mutex;
char str[10] = {'c', 'i', 'a', 'o', 'c','o','m','e','v','a'};

void buffer_init(char * buffer){
	int i;
	for (int i = 0; i < 10; i++)
		buffer[i]='X';
}

void *read_data(void * args){
	printf("trying to acquire readlock...\n");
	rwlock_acquire_readlock(&rwlock);
	printf("readlock acquired.\n");
	args_t *a = (args_t*)args;
	int i = a->thnum;
	char * buf = a->array;printf("*** reader thread %d ***\n", i);
	int j;
	for (j = 0; j < 10; j++){
		printf("%c", buf[j]);
	}
	printf("\n");
	rwlock_release_readlock(&rwlock);
}

void *write_data(void * args){
	printf("trying to acquire writelock...\n");
	rwlock_acquire_writelock(&rwlock);
	printf("writelock acquired.\n");
	args_t *a = (args_t*)args;
	int i = a->thnum;
	char * buf = a->array;
	printf("*** writer thread %d ***\n", i);
	buf[i] = str[i];
	rwlock_release_writelock(&rwlock);
}

int main(int argc, char * argv[]){
	char buffer[10];
	pthread_t reader[10];
	pthread_t writer[10];

	buffer_init(buffer);
	rwlock_init(&rwlock);
	sem_init(&mutex, 0, 1);
	int i;
	for (i = 0; i < 10; i++){
		args_t *args = malloc(sizeof(args_t));
		args->thnum = i;
		args->array = buffer;
		printf("spawning reader %d\n", i);
		pthread_create(&reader[i], 0, &read_data, args);	
		printf("spawning writer %d\n", i);
		pthread_create(&writer[i], 0, &write_data, args);
	}
	for (i = 0; i < 10; i++){
		pthread_join(reader[i], 0);
		pthread_join(writer[i], 0);
	}
	args_t *args = malloc(sizeof(args_t));
	args->array = buffer;
	args->thnum = 11;
	pthread_t final_reader;
	pthread_create(&final_reader, 0, &read_data, args);
	pthread_join(final_reader, 0);
	return 0;
}
