CC=gcc
BIN=bin/
SRC=src/
LIB=src/lib/

all: prova_rwlock

fair: fair_rwlock

fair_rwlock: $(SRC)prova_fair.c $(LIB)rwlock_fair.h
	$(CC) -o $(BIN)fair_rwlock $(SRC)prova_fair.c $(LIB)rwlock_fair.h -pthread

prova_rwlock: $(SRC)prova_rwlock.c $(LIB)rwlock.h
	$(CC) -o $(BIN)prova_rwlock $(LIB)rwlock.h $(SRC)prova_rwlock.c -pthread

.PHONY: clean

clean:
	rm $(BIN)fair_rwlock; rm $(BIN)prova_rwlock
